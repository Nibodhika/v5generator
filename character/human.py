from __future__ import annotations

from typing import List

from character.character import Character, register_character


@register_character("Human")
class Human(Character):

    @classmethod
    def random(cls,
               gender: GENDER = None,
               nationality: NATIONALITY = None,
               min_age: int = 20,
               max_age: int = 50,
               events_n: int or None = 1
               ):

        # Send an empty characteristic list so they don't get randomized twice
        c = Human({})
        c.randomize(gender, nationality, min_age, max_age, events_n)
        return c

    def randomize(self,
                  gender: GENDER = None,
                  nationality: NATIONALITY or List[Nationality] = None,
                  min_age: int = 20,
                  max_age: int = 50,
                  events_n: int or None = 1
                  ):
        from characteristics import Name, Age, Gender, HairColor, EyeColor, Height, Nationality
        from characteristics import BodyType, Attributes, Skills

        if gender is None:
            gender = Gender.random()
        else:
            gender = Gender(gender)

        self.add(gender)

        if nationality is None:
            nationality = Nationality.random()
        elif isinstance(nationality, list):
            nationality = Nationality.random(nationality)
        else:
            nationality = Nationality(nationality)

        self.add(nationality)

        age = Age.random(min_age, max_age)
        self.add(age)

        name = Name.random(gender, nationality)
        self.add(name)

        hair = HairColor.random()
        self.add(hair)

        eyes = EyeColor.random()
        self.add(eyes)

        height = Height.random(gender)
        self.add(height)

        body_type = BodyType.random()
        self.add(body_type)

        attributes = Attributes.random()
        self.add(attributes)

        skills = Skills.random(attributes=attributes.value)
        self.add(skills)

        if events_n is not None:
            self.generate_events(events_n)

    def generate_events(self, events_n):
        from characteristics import EventsCharacteristics
        events = EventsCharacteristics({})
        self.add(events)

        for i in range(events_n):
            events.generate(self)

    def add_event(self, name, event):
        self.events[name] = event

    def has_event(self, name):
        # This might be called before events is actually added
        if 'Events' in self._characteristics:
            return name in self.events
        return False

    @property
    def story(self):
        out = self.description
        if self.events:
            out += "\n" + " ".join([e.txt for e in self.events.values()])
        return out

    def get_biography_grammar(self):
        out = {}

        body_type = self.body_type.apparence_grammar(self)

        out.update({
            "born": [
                "#Name# was born in #Nationality_country# #Age# years ago.",
                "#Name# is a #Age# years old #Nationality#.",
            ],
            "hair_eyes": [
                "#they.capitalize# has #HairColor# hair and #EyeColor# eyes.",
                "#their.capitalize# hair is #HairColor# and #their# eyes are #EyeColor#.",
            ],
            "height": [
                "#they.capitalize# is #Height#m tall"
            ],
            "body_type": body_type,
            "origin": ["#born# #hair_eyes# #height#, #body_type#."]
        })
        return out

    @property
    def description(self):
        import tracery
        from tracery.modifiers import base_english

        rules = self.get_base_grammar()
        rules.update(self.get_biography_grammar())

        grammar = tracery.Grammar(rules)
        grammar.add_modifiers(base_english)

        return grammar.flatten('#origin#')

