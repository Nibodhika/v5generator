from __future__ import annotations

from character import Human
from character.character import register_character


@register_character("Vampire")
class Vampire(Human):

    @classmethod
    def random(cls,
               gender: GENDER = None,
               nationality: NATIONALITY = None,
               min_age: int = 20,
               max_age: int = 50,
               clan: CLAN = None,
               v_years_min: int = 0,
               v_years_max: int = 15,
               events_n: int or None = 1
               ):

        # Send an empty characteristic list so they don't get randomized twice
        v = Vampire({})
        v.randomize(gender, nationality, min_age, max_age, clan, events_n=events_n)
        return v

    def randomize(self,
                  gender: GENDER = None,
                  nationality: NATIONALITY = None,
                  min_age: int = 20,
                  max_age: int = 50,
                  clan: CLAN = None,
                  v_years_min: int = 0,
                  v_years_max: int = 15,
                  events_n: int or None = 1
                  ):
        from characteristics import Clan
        from characteristics import YearsAsVampire

        # pass None as events_n to avoid generating events
        super().randomize(gender, nationality, min_age, max_age, events_n=None)

        if clan is None:
            clan = Clan.random(self)
        else:
            clan = Clan(clan)
        self.add(clan)

        v_years = YearsAsVampire.random(v_years_min, v_years_max)
        self.set("Age", self.get("Age") + v_years.value)
        self.add(v_years)

        # Add the events after generating everything else
        self.generate_events(events_n)

    def get_base_grammar(self):
        rules = super().get_base_grammar()
        rules["Age Embraced"] = str(self.get("Age") - self.get("Years as Vampire"))
        return rules

    def get_biography_grammar(self):
        out = super().get_biography_grammar()

        extended = {
            "embraced": [
                "#they.capitalize# was embraced #Years as Vampire# years ago into clan #Clan#",
                "#Years as Vampire# years ago #they# was embraced as a #Clan#",
            ],
            "age_apparence": [
                "#they# appears to be #Age Embraced# years of age",
                "#their# apparence froze at #Age Embraced# years old"
            ]
        }
        new_origin = []
        for o in out["origin"]:
            no = o + "\n#embraced#, #age_apparence#"
            new_origin.append(no)
        out["origin"] = new_origin

        out.update(extended)
        return out
