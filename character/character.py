from __future__ import annotations

from typing import List, Dict

_registry = {}


class Character(object):

    def __init__(self,
                 characteristics: dict = None,
                 tags: List[str] = None,
                 events: Dict[str, Event] = None
                 ):

        if characteristics is None:
            self._characteristics = {}
            self.randomize()
        else:
            self._characteristics = characteristics.copy()

        self.tags = tags if tags is not None else []
        self.events = events if events is not None else {}

    def __repr__(self):
        from characteristics import Name
        return '<{}: {}>'.format(self.NAME, self.get(Name.name))

    def randomize(self):
        raise NotImplementedError()

    def get(self, name):
        try:
            return self._characteristics[name].value
        except KeyError:
            return None

    def set(self, name, value):
        try:
            self._characteristics[name].value = value
            return value
        except KeyError:
            return None

    def __getitem__(self, item):
        return self.get(item)

    def add(self, characteristic: Characteristic):
        self._characteristics[characteristic.name] = characteristic

    def remove(self, name):
        try:
            del self._characteristics[name]
        except KeyError:
            pass

    def as_dict(self):
        return {k: v.value for k, v in self._characteristics.items()}

    def as_json(self):
        return {k: v.json_value for k, v in self._characteristics.items()}

    def from_dict(self, d):
        self._characteristics = d.copy()

    def get_base_grammar(self):

        rules = {}
        for c in self._characteristics.values():
            rules.update(c.grammar)

        return rules

    @classmethod
    def create(cls, type: str, *args, **kwargs):
        try:
            return _registry[type](*args, **kwargs)
        except KeyError:
            return None


def register_character(name: str):
    def _internal(cls):
        cls.NAME = name
        _registry[name] = cls
        return cls
    return _internal
