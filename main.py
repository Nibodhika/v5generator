import json

from character.vampire import Vampire

v = Vampire.random(events_n=2)
print(json.dumps(v.as_json(), indent=4))
print()
print(v.story)
