# (Optional) Virtualenv setup

Create and activate the virtualenv

```bash
virtualenv venv -p python3
source venv/bin/activate
```
# Install requirements

```bash
pip install -r requirements.txt
```

# Generating a random vampire

```bash
python ./main.py
```
