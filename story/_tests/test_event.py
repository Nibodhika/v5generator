

def test_meets_requirements():
    from character import Human
    from characteristics import ATTRIBUTES, SKILLS
    from story.event import EventFactory

    h = Human()
    h2 = Human()

    # Test requirements of attributes
    h.attributes[ATTRIBUTES.STRENGTH] = 2
    event_name = "Test Event"
    ef = EventFactory(event_name, {}, ['grammar'], {'_attributes': {'Strength': 2}})
    assert ef.meets_requirements(h)
    h.attributes[ATTRIBUTES.STRENGTH] = 1
    assert not ef.meets_requirements(h)

    # Creates an event and adds it to the character
    e = ef.create(h)
    assert event_name in h.events
    assert h.events[event_name] is e

    # Test requirements of events
    ef2 = EventFactory("Test Event2", {}, ['grammar'], {'_events': [event_name]})
    assert ef2.meets_requirements(h)
    assert not ef2.meets_requirements(h2)

    # Test requirements of skill
    h.skills[SKILLS.ATHLETICS] = 3
    ef3 = EventFactory("require athletics", {}, ['grammar'], {'_skills': {'Athletics': 3}})
    assert ef3.meets_requirements(h)
    h.skills[SKILLS.ATHLETICS] = 2
    assert not ef3.meets_requirements(h)

    # Test Multiple events requirement
    ef4 = EventFactory("multiple events", {}, ['grammar'], {'_events': [event_name, "Test Event2"]})
    assert not ef4.meets_requirements(h)
    ef2.create(h)
    assert ef4.meets_requirements(h)

    # Test maximum/min value
    ef5 = EventFactory("require athletics", {}, ['grammar'], {'_skills': {'Athletics': {'_max': 3, '_min': 1}}})
    assert ef5.meets_requirements(h)
    h.skills[SKILLS.ATHLETICS] = 3
    assert ef5.meets_requirements(h)
    h.skills[SKILLS.ATHLETICS] = 4
    assert not ef5.meets_requirements(h)
    h.skills[SKILLS.ATHLETICS] = 1
    assert ef5.meets_requirements(h)
    h.skills[SKILLS.ATHLETICS] = 0
    assert not ef5.meets_requirements(h)

