

def get_weighted(list):
    out = {}
    for i, l in enumerate(list):
        probability = 1
        if '_probability' in l:
            probability = l['_probability']
        out[i] = probability
    return out


def choose_weighted(list):
    from characteristics.helpers import weighted_choice
    weighted_list = get_weighted(list)
    i = weighted_choice(weighted_list)
    return list[i]


def path2list(path):
    if isinstance(path, str):
        return path.split('.')
    return path


def flatten_dict(d, base_key=None):
    out = {}
    for k, v in d.items():
        if base_key is None:
            key = k
        else:
            key = '{}_{}'.format(base_key, k)
        if isinstance(v, dict):
            v = flatten_dict(v, key)
            out.update(v)
        else:
            out[key] = v
    return out
