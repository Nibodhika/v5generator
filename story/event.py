from __future__ import annotations

import copy
import json
import os
import random
from typing import Dict, List

_registry = {}


class Event(object):

    def __init__(self, character: Human = None, values: Dict = {}, txt: str = ""):
        self.character = character
        self.values = values
        self.txt = txt

    @classmethod
    def random(cls, character):
        available_events = []
        for e in _registry.values():
            if e.meets_requirements(character) and not character.has_event(e.name):
                available_events.append(e)

        # TODO define some rules
        generator = random.choice(available_events)
        return generator.name, generator(character)


class EventFactory(object):
    def __init__(self, name: str = "", rules: Dict = {}, grammar: List = [], requirements: Dict = {}):
        self.name = name
        self.rules = rules
        self.grammar = grammar
        self.requirements = requirements

    def meets_requirements(self, character):
        if len(self.requirements) == 0:
            return True

        for key, requires in self.requirements.items():
            d = character.get(key)
            if d is None:
                return False

            if isinstance(requires, list):
                for r in requires:
                    if r not in d:
                        return False
            elif isinstance(requires, dict):
                for r, v in requires.items():
                    if r not in d:
                        return False
                    if isinstance(v, int):
                        if d[r] < v:
                            return False
                    elif isinstance(v, dict):
                        if '_min' in v:
                            if d[r] < v['_min']:
                                return False
                        if '_max' in v:
                            if d[r] > v['_max']:
                                return False

        return True

    def create(self, character):
        values = self.random_values()
        txt = self.generate_text(character, values)
        e = Event(character, values, txt)
        # character.add_event(self.name, e)
        return e

    def __call__(self, character):
        return self.create(character)

    def random_values(self):
        values = {}
        for r, path in self.rules.items():
            element = self.get_random(path)
            values[r] = element
        return values

    @staticmethod
    def get_random(path):
        from story.helpers import path2list, choose_weighted

        path = path2list(path)
        elements = ELEMENTS
        for p in path:
            elements = elements[p]

        out = choose_weighted(copy.deepcopy(elements))
        if isinstance(out, dict):
            out_keys = list(out.keys())
            for k in out_keys:
                if k.startswith('_'):
                    out.pop(k)
        return out

    def generate_text(self, character, values):
        import tracery
        from tracery.modifiers import base_english
        from story.helpers import flatten_dict

        rules = character.get_base_grammar()
        rules.update(flatten_dict(values))

        if 'Events' in self.requirements:
            for r in self.requirements['Events']:
                rules.update(flatten_dict(character.events[r].values))

        rules.update({'_grammar': self.grammar})

        grammar = tracery.Grammar(rules)
        grammar.add_modifiers(base_english)
        return grammar.flatten('#{}#'.format('_grammar'))

    def load_random(self, key, path, elem_key=None):
        element = self.get_random(path)
        if elem_key is not None:
            element = element[elem_key]
        self.values[key] = element


def register_events(list):
    for l in list:
        name = l['_name']
        rules = l['_rules']
        grammar = l['_grammar']
        requirements = {}
        if '_requires' in l:
            requirements = l['_requires']
        _registry[name] = EventFactory(name, rules, grammar, requirements)


ELEMENTS_FOLDER = os.path.join(os.path.dirname(__file__), "elements")
ELEMENTS = {}

for e in os.listdir(ELEMENTS_FOLDER):
    name, ext = os.path.splitext(e)
    if ext.lower() == ".json":
        with open(os.path.join(ELEMENTS_FOLDER, e), 'r') as ifs:
            data = json.load(ifs)
        if '_events' in data:
            events = data.pop('_events')
            register_events(events)
        ELEMENTS[name] = data