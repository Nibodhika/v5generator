import random
from enum import Enum

import pytest


@pytest.fixture
def enum_characteristic():
    from characteristics import EnumCharacteristic

    class TestEnum(Enum):
        A = 1
        B = 2

    class TestChar(EnumCharacteristic):
        ENUM = TestEnum

    yield TestEnum, TestChar


@pytest.fixture
def int_characteristic():
    from characteristics import IntCharacteristic

    class TestInt(IntCharacteristic):
        MIN = -10
        MAX = 10

        DEFAULT_MIN = -1
        DEFAULT_MAX = 10

    return TestInt


def test_enum_characteristic_validator(enum_characteristic):
    TestEnum, TestChar = enum_characteristic

    assert TestChar.validator(TestEnum.A) == TestEnum.A
    assert TestChar.validator(2) == TestEnum.B

    with pytest.raises(ValueError):
        TestChar.validator(3)


def test_enum_characteristic_random(enum_characteristic, mocker):
    TestEnum, TestChar = enum_characteristic

    rand = mocker.patch.object(random, 'choice', return_value=TestEnum.B)

    t = TestChar.random()
    rand.assert_called_once_with(list(TestEnum))
    assert t.value == TestEnum.B


def test_int_characteristic_validator(int_characteristic):

    assert int_characteristic.validator(0) == 0
    assert int_characteristic.validator(-10) == -10

    # Below minimum
    with pytest.raises(ValueError):
        int_characteristic.validator(-11)

    assert int_characteristic.validator(10) == 10

    # Above maximum
    with pytest.raises(ValueError):
        int_characteristic.validator(11)

    # Not an integer
    with pytest.raises(ValueError):
        int_characteristic.validator('wrong')

    # Floats are clamped
    assert int_characteristic.validator(2.9) == 2


def test_int_characteristic_random(int_characteristic, mocker):

    rand = mocker.patch.object(random, 'randint', return_value=5)

    t = int_characteristic.random()
    rand.assert_called_once_with(int_characteristic.DEFAULT_MIN, int_characteristic.DEFAULT_MAX)
    assert t.value == 5
