import random

from characteristics.sheet import Attributes, ATTRIBUTES, Skills, SKILLS


def test_attributes():

    default = Attributes({})
    for a in ATTRIBUTES:
        assert a in default.value
        assert default.value[a] == 1

    str_2 = Attributes({ATTRIBUTES.STRENGTH: 2})
    for a in ATTRIBUTES:
        assert a in str_2.value
        if a == ATTRIBUTES.STRENGTH:
            assert str_2.value[a] == 2
        else:
            assert str_2.value[a] == 1

    dex_2 = Attributes({"Dexterity": 2})
    for a in ATTRIBUTES:
        assert a in dex_2.value
        if a == ATTRIBUTES.DEXTERITY:
            assert dex_2.value[a] == 2
        else:
            assert dex_2.value[a] == 1


def test_skills():

    default = Skills({})
    for s in SKILLS:
        assert s in default.value
        assert default.value[s] == 0

    brawl_2 = Skills({SKILLS.BRAWL: 2})
    for s in SKILLS:
        assert s in brawl_2.value
        if s == SKILLS.BRAWL:
            assert brawl_2.value[s] == 2
        else:
            assert brawl_2.value[s] == 0

    occult_3 = Skills({"Occult": 3})
    for s in SKILLS:
        assert s in occult_3.value
        if s == SKILLS.OCCULT:
            assert occult_3.value[s] == 3
        else:
            assert occult_3.value[s] == 0
