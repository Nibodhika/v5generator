import numpy as np
import pytest


def test_random_height(mocker):
    from characteristics import Height, Gender, GENDER

    normal = mocker.spy(np.random, 'normal')
    Height.random(Gender(GENDER.MALE))
    normal.assert_called_once_with(*Height.MALE)

    normal.reset_mock()
    Height.random(Gender(GENDER.FEMALE))
    normal.assert_called_once_with(*Height.FEMALE)

    normal.reset_mock()
    average = (Height.MALE[0] + Height.FEMALE[0]) / 2.0
    deviation = (Height.MALE[1] + Height.FEMALE[1]) / 2.0
    Height.random()
    normal.assert_called_once_with(average, deviation)


def test_height():
    from characteristics import Height

    with pytest.raises(TypeError):
        height = Height()

    with pytest.raises(ValueError):
        height = Height('wrong')

    with pytest.raises(ValueError):
        height = Height(-1)

    height = Height(2)
    assert height.value == 2
