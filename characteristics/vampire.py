from characteristics import register_characteristic
from characteristics.generics import EnumCharacteristic, ValueEnum, IntCharacteristic


class CLAN(ValueEnum):
    BRUJAH = 'Brujah'
    GANGREL = 'Gangrel'
    MALKAVIAN = 'Malkavian'
    NOSFERATU = 'Nosferatu'
    TOREADOR = 'Toreador'
    TREMERE = 'Tremere'
    VENTRUE = 'Ventrue'

    def probability(self, character):
        from characteristics import SKILLS
        if self is CLAN.BRUJAH:
            skills = (SKILLS.MELEE, SKILLS.INTIMIDATION)
        elif self is CLAN.GANGREL:
            skills = (SKILLS.SURVIVAL, SKILLS.BRAWL)
        elif self is CLAN.MALKAVIAN:
            skills = (SKILLS.AWARENESS, SKILLS.INSIGHT)
        elif self is CLAN.NOSFERATU:
            skills = (SKILLS.STEALTH, SKILLS.SUBTERFUGE)
        elif self is CLAN.TOREADOR:
            skills = (SKILLS.CRAFT, SKILLS.PERFORMANCE)
        elif self is CLAN.TREMERE:
            skills = (SKILLS.OCCULT, SKILLS.ACADEMICS)
        elif self is CLAN.VENTRUE:
            skills = (SKILLS.POLITICS, SKILLS.LEADERSHIP)
        else:
            raise ValueError("Clan {} has no attached skills".format(self))

        c_skills = character.skills

        out = 1
        for s in skills:
            out += c_skills[s]
        return out


@register_characteristic("Clan")
class Clan(EnumCharacteristic):
    ENUM = CLAN

    @classmethod
    def random(cls, character):
        from characteristics.helpers import weighted_choice

        clans = {k: k.probability(character) for k in list(CLAN)}
        clan = weighted_choice(clans)
        return Clan(CLAN(clan))


@register_characteristic("Years as Vampire")
class YearsAsVampire(IntCharacteristic):
    MIN = 0

    DEFAULT_MIN = 0
    DEFAULT_MAX = 15
