from enum import Enum
import random
from typing import List

from characteristics import register_characteristic, Characteristic, ValueEnum
from characteristics.generics import EnumListCharacteristic


class ATTRIBUTES(ValueEnum):
    # Physical
    STRENGTH = "Strength"
    DEXTERITY = "Dexterity"
    STAMINA = "Stamina"
    # Social
    CHARISMA = "Charisma"
    MANIPULATION = "Manipulation"
    COMPOSURE = "Composure"
    # Mental
    INTELLIGENCE = "Intelligence"
    WITS = "Wits"
    RESOLVE = "Resolve"


@register_characteristic("Attributes")
class Attributes(EnumListCharacteristic):
    ENUM = ATTRIBUTES
    DEFAULT_VALUE = 1

    @classmethod
    def random(cls):
        return super().random(points_distribution=[4, 3, 3, 3, 2, 2, 2, 2, 1], probability_distribution=None)


class SKILLS(ValueEnum):
    # Physical
    ATHLETICS = "Athletics"
    BRAWL = "Brawl"
    CRAFT = "Craft"
    DRIVE = "Drive"
    FIREARMS = "Firearms"
    LARCENY = "Larceny"
    MELEE = "Melee"
    STEALTH = "Stealth"
    SURVIVAL = "Survival"
    # Social
    ANIMAL_KEN = "Animal Ken"
    ETIQUETTE = "Etiquette"
    INSIGHT = "Insight"
    INTIMIDATION = "Intimidation"
    LEADERSHIP = "Leadership"
    PERFORMANCE = "Performance"
    PERSUASION = "Persuasion"
    STREETWISE = "Streetwise"
    SUBTERFUGE = "Subterfuge"
    # Mental
    ACADEMICS = "Academics"
    AWARENESS = "Awareness"
    FINANCE = "Finance"
    INVESTIGATION = "Investigation"
    MEDICINE = "Medicine"
    OCCULT = "Occult"
    POLITICS = "Politics"
    SCIENCE = "Science"
    TECHNOLOGY = "Technology"

    def probability(self, attributes):
        # Physical
        if self is SKILLS.ATHLETICS:
            attr = (ATTRIBUTES.DEXTERITY, ATTRIBUTES.STAMINA)
        elif self is SKILLS.BRAWL:
            attr = (ATTRIBUTES.STRENGTH, ATTRIBUTES.DEXTERITY)
        elif self is SKILLS.CRAFT:
            attr = (ATTRIBUTES.DEXTERITY, ATTRIBUTES.INTELLIGENCE)
        elif self is SKILLS.DRIVE:
            attr = (ATTRIBUTES.DEXTERITY, ATTRIBUTES.WITS)
        elif self is SKILLS.FIREARMS:
            attr = (ATTRIBUTES.DEXTERITY, ATTRIBUTES.WITS)
        elif self is SKILLS.LARCENY:
            attr = (ATTRIBUTES.DEXTERITY, ATTRIBUTES.WITS)
        elif self is SKILLS.MELEE:
            attr = (ATTRIBUTES.DEXTERITY, ATTRIBUTES.STRENGTH)
        elif self is SKILLS.STEALTH:
            attr = (ATTRIBUTES.DEXTERITY, ATTRIBUTES.WITS)
        elif self is SKILLS.SURVIVAL:
            attr = (ATTRIBUTES.WITS, ATTRIBUTES.INTELLIGENCE)
        # Social
        elif self is SKILLS.ANIMAL_KEN:
            attr = (ATTRIBUTES.WITS, ATTRIBUTES.CHARISMA)
        elif self is SKILLS.ETIQUETTE:
            attr = (ATTRIBUTES.CHARISMA, ATTRIBUTES.COMPOSURE)
        elif self is SKILLS.INSIGHT:
            attr = (ATTRIBUTES.WITS, ATTRIBUTES.MANIPULATION)
        elif self is SKILLS.INTIMIDATION:
            attr = (ATTRIBUTES.MANIPULATION, ATTRIBUTES.WITS)
        elif self is SKILLS.LEADERSHIP:
            attr = (ATTRIBUTES.CHARISMA, ATTRIBUTES.MANIPULATION)
        elif self is SKILLS.PERFORMANCE:
            attr = (ATTRIBUTES.CHARISMA, ATTRIBUTES.COMPOSURE)
        elif self is SKILLS.PERSUASION:
            attr = (ATTRIBUTES.CHARISMA, ATTRIBUTES.WITS)
        elif self is SKILLS.STREETWISE:
            attr = (ATTRIBUTES.INTELLIGENCE, ATTRIBUTES.WITS)
        elif self is SKILLS.SUBTERFUGE:
            attr = (ATTRIBUTES.DEXTERITY, ATTRIBUTES.INTELLIGENCE)
        # Mental
        elif self is SKILLS.ACADEMICS:
            attr = (ATTRIBUTES.INTELLIGENCE, ATTRIBUTES.WITS)
        elif self is SKILLS.AWARENESS:
            attr = (ATTRIBUTES.WITS, ATTRIBUTES.RESOLVE)
        elif self is SKILLS.FINANCE:
            attr = (ATTRIBUTES.INTELLIGENCE, ATTRIBUTES.RESOLVE)
        elif self is SKILLS.INVESTIGATION:
            attr = (ATTRIBUTES.INTELLIGENCE, ATTRIBUTES.WITS)
        elif self is SKILLS.MEDICINE:
            attr = (ATTRIBUTES.INTELLIGENCE, ATTRIBUTES.DEXTERITY)
        elif self is SKILLS.OCCULT:
            attr = (ATTRIBUTES.INTELLIGENCE, ATTRIBUTES.WITS)
        elif self is SKILLS.POLITICS:
            attr = (ATTRIBUTES.INTELLIGENCE, ATTRIBUTES.WITS)
        elif self is SKILLS.SCIENCE:
            attr = (ATTRIBUTES.INTELLIGENCE, ATTRIBUTES.WITS)
        elif self is SKILLS.TECHNOLOGY:
            attr = (ATTRIBUTES.INTELLIGENCE, ATTRIBUTES.WITS)
        else:
            raise ValueError("Unknown skill {}".format(self))

        return attributes[attr[0]] + attributes[attr[1]]


class SKILL_DISTRIBUTION(Enum):
    JACK_OF_ALL_TRADES = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    BALANCED = [3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1]
    SPECIALIST = [4, 3, 3, 3, 2, 2, 2, 1, 1, 1]


@register_characteristic("Skills")
class Skills(EnumListCharacteristic):
    ENUM = SKILLS
    DEFAULT_VALUE = 0

    @classmethod
    def random(cls,
               points_distribution: List[int] = None,
               probability_distribution=None,
               attributes=None,
               skill_distribution: SKILL_DISTRIBUTION = None
               ):

        if points_distribution is None:
            points_distribution = random.choice(list(SKILL_DISTRIBUTION)).value

        if probability_distribution is None and attributes is not None:
            probability_distribution = {k: k.probability(attributes) for k in list(SKILLS)}

            # Mental characters are usually focused on one or two areas, try but the probability is the same for all
            # so a character with high int and wits
            # is likely to get high indices of academics, occult, politics science and technology
            # Select 3 of them at random and diminish the probability, so that he's likely to be specialized in 2 areas
            if SKILLS.ACADEMICS.probability(attributes) > 6:
                remove_from = random.choices([
                    SKILLS.ACADEMICS, SKILLS.OCCULT, SKILLS.POLITICS, SKILLS.SCIENCE, SKILLS.TECHNOLOGY
                ], k=3)
                for s in remove_from:
                    probability_distribution[s] -= 3


        return super().random(points_distribution, probability_distribution)

