from __future__ import annotations

from characteristics import Characteristic, register_characteristic
from characteristics.generics import WeightedCharacteristic, EnumCharacteristic, ValueEnum, IntCharacteristic


class GENDER(ValueEnum):
    MALE = 'Male'
    FEMALE = 'Female'

    @property
    def they(self):
        if self is GENDER.MALE:
            return "he"
        return "she"

    @property
    def them(self):
        if self is GENDER.MALE:
            return "him"
        return "her"

    @property
    def their(self):
        if self is GENDER.MALE:
            return "his"
        return "her"

    @property
    def theirs(self):
        if self is GENDER.MALE:
            return "his"
        return "hers"


@register_characteristic("Gender")
class Gender(EnumCharacteristic):
    ENUM = GENDER

    def _grammar(self):
        return {
            "Gender": str(self.value),
            "they": self.value.they,
            "them": self.value.them,
            "their": self.value.their,
            "theirs": self.value.theirs
        }


class NATIONALITY(ValueEnum):
    ar_EG = 'Egypsian'
    bs_BA = 'Bosnian'
    cs_CZ = 'Czech'
    de_DE = 'German'
    dk_DK = "Danish"
    en_AU = "Australian"
    en_CA = "Canadian"
    en_GB = "British"
    en_NZ = "New Zealander"
    en_US = 'American'
    es_ES = "Spanish"
    es_MX = "Mexican"
    et_EE = "Estonian"
    fi_FI = "Finnish"
    fr_FR = "French"
    hr_HR = "Croatian"
    hu_HU = "Hungarian"
    it_IT = 'Italian'
    lt_LT = "Lithuanian"
    nl_NL = "Dutch"
    no_NO = "Norwegian"
    pl_PL = "Polish"
    pt_BR = 'Brazilian'
    pt_PT = "Portuguese"
    ro_RO = "Romanian"
    sl_SI = "Slovene"
    sv_SE = "Swedish"
    tr_TR = "Turkish"

    # These use especial characters
    # ar_PS = Arabic (Palestine)
    # ar_SA = Arabic (Saudi Arabia)
    # bg_BG = Bulgarian
    # el_GR = Greek
    # fa_IR = Persian (Iran)
    # hi_IN = Hindi
    # hy_AM = Armenian
    # ja_JP = Japanese
    # ka_GE = Georgian (Georgia)
    # ko_KR = Korean
    # lv_LV = Latvian
    # ne_NP = Nepali
    # ru_RU = Russian
    # uk_UA = Ukrainian
    # zh_CN = Chinese (China)
    # zh_TW = Chinese (Taiwan)

    @property
    def country(self):

        if self is NATIONALITY.ar_EG:
            return 'Egypt'
        elif self is NATIONALITY.bs_BA:
            return 'Bosnia'
        elif self is NATIONALITY.cs_CZ:
            return 'Czech Republic'
        elif self is NATIONALITY.de_DE:
            return 'Germany'
        elif self is NATIONALITY.dk_DK:
            return "Denmark"
        elif self is NATIONALITY.en_AU:
            return "Australia"
        elif self is NATIONALITY.en_CA:
            return "Canada"
        elif self is NATIONALITY.en_GB:
            return "Great Britain"
        elif self is NATIONALITY.en_NZ:
            return "New Zealand"
        elif self is NATIONALITY.en_US:
            return 'United States'
        elif self is NATIONALITY.es_ES:
            return "Spain"
        elif self is NATIONALITY.es_MX:
            return "Mexico"
        elif self is NATIONALITY.et_EE:
            return "Estonia"
        elif self is NATIONALITY.fi_FI:
            return "Finland"
        elif self is NATIONALITY.fr_FR:
            return "France"
        elif self is NATIONALITY.hr_HR:
            return "Croatia"
        elif self is NATIONALITY.hu_HU:
            return "Hungary"
        elif self is NATIONALITY.it_IT:
            return 'Italy'
        elif self is NATIONALITY.lt_LT:
            return "Lithuania"
        elif self is NATIONALITY.nl_NL:
            return "Netherlands"
        elif self is NATIONALITY.no_NO:
            return "Norway"
        elif self is NATIONALITY.pl_PL:
            return "Poland"
        elif self is NATIONALITY.pt_BR:
            return 'Brazil'
        elif self is NATIONALITY.pt_PT:
            return "Portugal"
        elif self is NATIONALITY.ro_RO:
            return "Romania"
        elif self is NATIONALITY.sl_SI:
            return "Slovenia"
        elif self is NATIONALITY.sv_SE:
            return "Sweden"
        elif self is NATIONALITY.tr_TR:
            return "Turkey"

        raise NotImplementedError("Nationality {} needs to implement country".format(self))


@register_characteristic("Nationality")
class Nationality(EnumCharacteristic):
    ENUM = NATIONALITY

    def _grammar(self):
        return {
            "Nationality": str(self.value),
            "Nationality_country": self.value.country
        }


class BODY_TYPE(ValueEnum):
    ECTOMORPH = "Ectomorph"
    MESOMORPH = "Mesomorph"
    ENDOMORPH = "Endomorph"

    def apparence_grammar(self, character: Human):
        from characteristics import ATTRIBUTES
        out = []
        try:
            is_strong = character.attributes[ATTRIBUTES.STRENGTH] >= 3
        except ValueError:
            is_strong = False

        if is_strong and self is BODY_TYPE.MESOMORPH:
            and_but = "and muscular"
        elif not is_strong and self is BODY_TYPE.MESOMORPH:
            and_but = "but weak"
        elif is_strong:
            and_but = "but muscular"
        else:
            and_but = "and weak"

        if self is BODY_TYPE.ECTOMORPH:
            out = [
                "#they# is thin {}".format(and_but),
                "#they# is skinny {}".format(and_but)
            ]
        elif self is BODY_TYPE.MESOMORPH:
            out = [
                "#they# is athletic {}".format(and_but),
            ]
        elif self is BODY_TYPE.ENDOMORPH:
            out = [
                "#they# is overweight {}".format(and_but),
            ]

        return out


@register_characteristic("Body Type")
class BodyType(EnumCharacteristic):
    ENUM = BODY_TYPE


@register_characteristic("Name")
class Name(Characteristic):

    @classmethod
    def validator(cls, value):
        if not isinstance(value, str):
            raise ValueError('"{}" is not a valid string'.format(value))
        return value

    @classmethod
    def random(cls, gender: Gender = None, nationality: Nationality = None):
        from faker import Faker

        fake = Faker(nationality.value.name)

        if gender.value is GENDER.MALE:
            name = fake.first_name_male()
            last_name = fake.last_name_male()
        elif gender.value is GENDER.FEMALE:
            name = fake.first_name_female()
            last_name = fake.last_name_female()
        else:
            name = fake.first_name()
            last_name = fake.last_name()

        name = "{} {}".format(name, last_name)
        return Name(name)


@register_characteristic("Age")
class Age(IntCharacteristic):
    MIN = 0

    DEFAULT_MIN = 20
    DEFAULT_MAX = 50


@register_characteristic("HairColor")
class HairColor(WeightedCharacteristic):
    # TODO put actual numbers based on population distribution
    ELEMENTS = {
        "Burnette": 0.7,
        "Blonde": 0.2,
        "Red": 0.1,
    }


@register_characteristic("EyeColor")
class EyeColor(WeightedCharacteristic):
    # TODO put actual numbers based on population distribution
    ELEMENTS = {
        "Brown": 0.7,
        "Green": 0.2,
        "Blue": 0.1,
    }


@register_characteristic("Height")
class Height(Characteristic):
    # Male and female average and deviation, source: https://ourworldindata.org/human-height
    MALE = [178.4, 7.59]
    FEMALE = [164.7, 7.07]

    @classmethod
    def validator(cls, value):
        try:
            value = float(value)
        except ValueError:
            raise ValueError('"{}" is not a valid Float'.format(value))

        if value < 0:
            raise ValueError('Height must be positive')

        return value

    @classmethod
    def random(cls, gender: Gender = None):
        from numpy.random import normal
        if gender is None:
            a = (cls.MALE[0] + cls.FEMALE[0]) / 2.0
            b = (cls.MALE[1] + cls.FEMALE[1]) / 2.0
        elif gender.value is GENDER.MALE:
            a, b = cls.MALE
        elif gender.value is GENDER.FEMALE:
            a, b = cls.FEMALE
        else:
            raise ValueError('Unknown gender "{}" for height distribution'.format(gender))

        return Height(round(normal(a, b) / 100, 2))


@register_characteristic("Events")
class EventsCharacteristics(Characteristic):

    def __repr__(self):
        return '<{}: {}>'.format(self.name, self._value.keys())

    @classmethod
    def validator(cls, value):
        from story.event import Event

        if not isinstance(value, dict):
            raise ValueError('"{}" should be a dictionary'.format(value))

        for k, v in value.items():
            if not isinstance(k, str) or not isinstance(v, Event):
                raise ValueError('"{}" should be a dictionary of string -> Event'.format(value))

        return value

    @property
    def json_value(self):
        return {k: v.txt for k, v in self._value.items()}

    @classmethod
    def random(cls):
        return cls({})

    def generate(self, character):
        from story.event import Event

        name, event = Event.random(character)
        self._value[name] = event


