

def normalize_dict(elements):
    """
    Normalizes the values of a dictionary (to be used as a probability distribution)

    :param elements: A dictionary where the values are numbers meant to be normalized
    :return: a dictionary with the normalized values
    """
    total = sum(elements.values())
    normalized = {}
    for k, v in elements.items():
        normalized[k] = v / total
    return normalized


def weighted_choice(choices):
    """
    Helper function to get a weighted choice from a dictionary
    where the keys are the element to choose from and the values are the probability

    :param choices: dictionary where the keys represent the elements to choose from
                    and the values the probability of each element

    :return: chosen element from the dict
    """
    from numpy.random import choice
    normalized = normalize_dict(choices)
    return choice(list(normalized.keys()), 1, p=list(normalized.values()))[0]


def weighted_shuffle(elements):
    """
    Helper function to get a shuffled list with probabilities for each element

    :param elements: dictionary where the keys represent the elements to choose from
                     and the values the probability of each element
    :return: a list in randomized order
    """
    from numpy.random import choice
    normalized = normalize_dict(elements)
    return list(choice(list(normalized.keys()), len(normalized), p=list(normalized.values()), replace=False))
