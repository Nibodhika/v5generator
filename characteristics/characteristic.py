from __future__ import annotations

_registry = {}


class Characteristic(object):
    name = None

    def __init__(self, value):
        self._value = None
        self.value = value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = self.validator(value)

    @property
    def json_value(self):
        return self._value

    @classmethod
    def validator(cls, value):
        raise NotImplementedError()

    @classmethod
    def random(cls):
        raise NotImplementedError()

    def _grammar(self):
        return {self.name: str(self.value)}

    @property
    def grammar(self):
        return self._grammar()

    def __repr__(self):
        return '<{}: {}>'.format(self.name, self.value)

    @classmethod
    def create(cls, characteristic: str, *args, **kwargs):
        try:
            return _registry[characteristic](*args, **kwargs)
        except KeyError:
            return None


def register_characteristic(name: str):
    def _internal(cls):
        from character import Character
        cls.name = name
        _registry[name] = cls

        attr_name = name.lower().replace(" ", "_")
        if hasattr(Character, attr_name):
            raise ValueError('Characteristic "{}" attr name ({}) collides with a preexisting characteristic'.replace(
                name, attr_name
            ))

        def getter(self):
            return self.get(name)

        def setter(self, value):
            return self.set(name, value)

        setattr(Character, attr_name, property(getter, setter))
        return cls
    return _internal
