from enum import Enum
from typing import List

from characteristics import Characteristic


class ValueEnum(str, Enum):
    """
    An enum that uses the value for repr and str
    """

    def __repr__(self):
        return self.value

    def __str__(self):
        return self.value


class EnumCharacteristic(Characteristic):
    """
    A Characteristic that must be chosen from a given enum
    """
    ENUM = None

    @property
    def json_value(self):
        return self._value.value

    @classmethod
    def validator(cls, value):
        try:
            return cls.ENUM(value)
        except ValueError:
            raise ValueError('"{}" is not a valid value for {}'.format(value, cls.name))

    @classmethod
    def random(cls, l: List = None):
        import random
        if l is None:
            l = list(cls.ENUM)
        g = random.choice(l)
        return cls(g)

    def _grammar(self):
        return {
            self.name: str(self.value)
        }


class IntCharacteristic(Characteristic):
    """
    A characteristic that is represented by an integer

    MIN and MAX are used for validation

    DEFAULT_MIN and DEFAULT_MAX are used for random generation
    """

    MIN = None
    MAX = None

    DEFAULT_MIN = 0
    DEFAULT_MAX = 100

    @classmethod
    def validator(cls, value):
        try:
            value = int(value)
        except ValueError:
            raise ValueError('"{}" is not a valid integer'.format(value))

        if cls.MIN is not None and value < cls.MIN:
            raise ValueError('"{}" lesser than the allowed minimum of {}'.format(value, cls.MIN))
        elif cls.MAX is not None and value > cls.MAX:
            raise ValueError('"{}" greater than the allowed maximum of {}'.format(value, cls.MAX))

        return value

    @classmethod
    def random(cls, min: int = None, max: int = None):
        import random
        min = min if min is not None else cls.DEFAULT_MIN
        max = max if max is not None else cls.DEFAULT_MAX
        return cls(random.randint(min, max))


class WeightedCharacteristic(Characteristic):
    """
    A Characteristic that pulls from a dictionary of weighted elements
    """
    ELEMENTS = None

    @classmethod
    def validator(cls, value):
        if value not in cls.ELEMENTS.keys():
            raise ValueError('"{}" not a valid element for {}'.format(value, cls.name))
        return value

    @classmethod
    def random(cls):
        from characteristics.helpers import weighted_choice
        return cls(weighted_choice(cls.ELEMENTS))


class EnumListCharacteristic(Characteristic):
    ENUM = None
    DEFAULT_VALUE = 0

    @property
    def json_value(self):
        out = {}
        for k, v in self.value.items():
            out[k.value] = v
        return out

    @classmethod
    def validator(cls, value):
        if not isinstance(value, dict):
            raise ValueError('"{}" should be a dictionary'.format(value))

        # Add default values and populate the dictionary with the values not created
        new_v = {}
        for a in cls.ENUM:
            if a in value:
                new_v[a] = value[a]
            elif a.value in value:
                new_v[a] = value[a.value]
            else:
                new_v[a] = cls.DEFAULT_VALUE

        return new_v

    @classmethod
    def random(cls, points_distribution: List[int] = None, probability_distribution=None):
        from characteristics.helpers import weighted_shuffle

        if probability_distribution is None:
            probability_distribution = {k: 1 for k in cls.ENUM}

        randomized = weighted_shuffle(probability_distribution)
        out = {}
        for k, v in zip(randomized, points_distribution):
            out[k] = v
        return cls(out)

    def _grammar(self):
        out = {}
        for k, v in self.value.items():
            out[k] = str(v)
        return out
